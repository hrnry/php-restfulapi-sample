<?php
/**
 * RESTful CRUD API
 */
class Rest{
  const DB_PATH = '/tmp/rest_data.sqlite';
  const DEFAULT_USER_NAME = 'admin';
  const DEFAULT_USER_PASS = 'nimda';
  const MAX_USER_NAME_LENGTH = 32;

  const HTTP_RESPONSE = array(
    200 => 'OK',
    201 => 'Created',

    400 => 'Bad Request',
    401 => 'Unauthorized',
    402 => 'Payment Required',
    403 => 'Forbidden',
    404 => 'Not Found',
    405 => 'Method Not Allowed',

    418 => 'I\'m a teapot',

    500 => 'Internal Server Error',
    501 => 'Not Implemented'
  );

  private $pdo = NULL;

  public function __construct(){
    $this->db_init();
  }

  //public function __destruct(){  }

  /**
   * ヘッダを設定し、jsonでレスポンスを返す
   * @param int $code
   * @param array $msg
   */
  private function send_response(int $code, $msg){
    header((isset(self::HTTP_RESPONSE[$code])) ?
      'HTTP/1.1 ' . strval($code) . ' ' . self::HTTP_RESPONSE[$code] : 'HTTP/1.1 500 Internal Server Error'
    );
    exit(json_encode(
      $msg,//(is_array($msg)) ? array_values($msg) : $msg,
      JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT
    ));
  }

  /**
   * データベース初期化
   */
  private function db_init(){
    //データベース接続
    $db_is_exists = file_exists(self::DB_PATH);
    $dsn = 'sqlite:' . self::DB_PATH;
    try{
      $this->pdo = new PDO($dsn);  //SQLite: ファイルが存在しない場合、この時点で作成される
      $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }catch(PDOException $e){
      $this->send_response(500, array('error' => $e->getMessage()));
    }

    if($db_is_exists === TRUE){
      return;
    }

    //データベース新規作成
    $this->pdo->query('CREATE TABLE users(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      name VARCHAR(' . self::MAX_USER_NAME_LENGTH . '),
      hash VARCHAR(255)
    )');
    $this->useradd(self::DEFAULT_USER_NAME, self::DEFAULT_USER_PASS);
    $this->send_response(201, array(
      'Created a new database' => array(
        'user' => self::DEFAULT_USER_NAME,
        'pass' => self::DEFAULT_USER_PASS
      )
    ));
  }

  /**
   * パスワード認証
   * @param string $user
   * @param string $pass
   * @return boolean 認証成功すれば true を返す
   */
  private function auth(?string $user, ?string $pass){
    if(! isset($user) || $user === ''){ return FALSE; }

    $stmt = $this->pdo->prepare('SELECT hash FROM users WHERE name = :user_name');
    $stmt->bindValue(':user_name', $user, PDO::PARAM_STR);
    $stmt->execute();
    $ret = $stmt->fetchAll();
    return (count($ret) === 0 || ! password_verify(($pass === NULL) ? '' : $pass, $ret[0]['hash'])) ? FALSE : TRUE;
  }

  /**
   * ユーザーの存在確認
   * @param string $user
   * @return boolean ユーザーが存在すれば true を返す
   */
  private function is_user_exists(?string $user){
    if(! isset($user) || $user === ''){ return FALSE; }

    $stmt = $this->pdo->prepare('SELECT COUNT(*) FROM users WHERE name = :user_name');
    $stmt->bindValue(':user_name', $user, PDO::PARAM_STR);
    $stmt->execute();
    $ret = $stmt->fetchColumn();
    return (intval($ret) === 0) ? FALSE : TRUE;
  }

  /**
   * ユーザー表示
   * @param string $user
   * @return array
   */
  private function userprint(?string $user){
    if(! isset($user) || $user === ''){
      //全ユーザー一覧
      $stmt = $this->pdo->query('SELECT id, name FROM users');
    }else{
      //ユーザー詳細
      $stmt = $this->pdo->prepare('SELECT id, name FROM users WHERE name = :user_name OR id = :user_id');
      $stmt->bindValue(':user_name', $user, PDO::PARAM_STR);
      $stmt->bindValue(':user_id', intval($user), PDO::PARAM_INT);
      $stmt->execute();
    }
    return $stmt->fetchAll();
  }

  /**
   * ユーザー追加
   * @param string $user
   * @param string $pass
   * @return boolean
   */
  private function useradd(?string $user, ?string $pass){
    if(! isset($user) || $user === ''){ return FALSE; }
    if($this->is_user_exists($user)){ return FALSE; }  //$user already exists
    if(mb_strlen($user) > self::MAX_USER_NAME_LENGTH){ return FALSE; }  //文字数オーバー

    $hash = password_hash(($pass === NULL) ? '' : $pass, PASSWORD_DEFAULT);
    $stmt = $this->pdo->prepare('INSERT INTO users(name, hash) VALUES (:user_name, :pass_hash)');
    $stmt->bindValue(':user_name', $user, PDO::PARAM_STR);
    $stmt->bindValue(':pass_hash', $hash, PDO::PARAM_STR);
    return $stmt->execute();
  }

  /**
   * パスワード変更
   * @param string $user
   * @param string $pass
   * @return boolean
   */
  private function usermod(?string $user, ?string $pass){
    if(! $this->is_user_exists($user)){ return FALSE; }  //$user does not exist

    $hash = password_hash(($pass === NULL) ? '' : $pass, PASSWORD_DEFAULT);
    $stmt = $this->pdo->prepare('UPDATE users SET hash = :pass_hash WHERE name = :user_name');
    $stmt->bindValue(':user_name', $user, PDO::PARAM_STR);
    $stmt->bindValue(':pass_hash', $hash, PDO::PARAM_STR);
    return $stmt->execute();
  }

  /**
   * ユーザー削除
   * @param string $user
   * @return boolean
   */
  private function userdel(?string $user){
    if(! $this->is_user_exists($user)){ return FALSE; }  //$user does not exist

    $stmt = $this->pdo->query('SELECT COUNT(*) FROM users');
    $ret = $stmt->fetchColumn();
    if(intval($ret) === 1){ return FALSE; }  //cannot delete last one

    $stmt = $this->pdo->prepare('DELETE FROM users WHERE name = :user_name');
    $stmt->bindValue(':user_name', $user, PDO::PARAM_STR);
    return $stmt->execute();
  }

  private function print_usage(){
    $this->send_response(404, array(
      'RESTful CRUD API Sample' => array(
        '/api/v1' => array(
          array('GET' => array(
            '/user' => 'List all users',
            '/user/[user name]' => 'Show user details'
          )),
          array('POST' => array('/user/[user name]/[password]' => 'Add new user')),
          array('PUT' => array('/user/[user name]/[password]' => 'Change password')),
          array('DELETE' => array('/user/[user name]' => 'Delete user'))
        )
      )
    ));
  }

  /**
   * handle_request
   * @param string $user
   * @param string $pass
   * @param string $method
   * @param string $uri
   */
  public function handle_request(?string $user, ?string $pass, ?string $method, ?string $uri){
    if(! isset($method)){
      $this->send_response(400, array('error' => 'Bad Request'));
    }

    $req_method = strtoupper($method);
    $url = parse_url($uri);
    $path = explode('/', $url['path']);  //$path[0] === ""
    if(count($path) < 4 || $path[1] !== 'api' || $path[2] !== 'v1' || $path[3] === ''){
      $this->print_usage();
    }
    $path = array_slice($path, 3);
    //$query = $url['query'];
    //$fragment = $url['fragment'];

    //認証
    if(! $this->auth($user, $pass)){
      $this->send_response(401, array('error' => 'Authentication Failed'));
    }

    $req_uri = array_shift($path);
    switch($req_uri){
      case 'user':  // .../api/v1/user
        $user = array_shift($path);
        $pass = array_shift($path);
        switch($req_method){
          case 'GET':  //Read: 取得
            $this->send_response(200, $this->userprint($user));
          case 'POST':  //Create: 作成, 追加
            if($this->useradd($user, $pass)){
              $this->send_response(200, array('useradd' => array('name' => $user)));
            }
            break;
          case 'PUT':  //Update: 更新
            if($this->usermod($user, $pass)){
              $this->send_response(200, array('usermod' => array('name' => $user)));
            }
            break;
          case 'DELETE':  //Delete: 削除
            if($this->userdel($user)){
              $this->send_response(200, array('userdel' => array('name' => $user)));
            }
            break;
          //HEAD,CONNECT,OPTIONS,TRACE,PATCH
          default:
            //$this->send_response(405, array('error' => "Request method not supported. ({$req_method})"));
            $this->print_usage();
        }
        $this->send_response(403, array('error' => 'Forbidden'));
      default:
        //$this->send_response(404, array('error' => "Not Implemented: {$req_method} {$req_uri}"));
        $this->print_usage();
    }

  }

}

?>
