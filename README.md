PHP-RESTfulAPI-sample
=====================

RESTful CRUD API Sample with PHP + SQLite(PDO) + BasicAuth


Usage
-----

```sh
php -S 127.0.0.1:8080 router.php
```

```sh
curl -v http://127.0.0.1:8080/ | jq
curl -v -u "admin:nimda" -X GET http://127.0.0.1:8080/api/v1/user/ | jq
```

| Method | http://127.0.0.1:8080/api/v1 | Operation         |
| ------ | ---------------------------- | ----------------- |
| GET    | /user                        | List all users    |
| GET    | /user/[user name]            | Show user details |
| POST   | /user/[user name]/[password] | Add new user      |
| PUT    | /user/[user name]/[password] | Change password   |
| DELETE | /user/[user name]            | Delete user       |
