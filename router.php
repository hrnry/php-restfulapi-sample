<?php
/**
 * RESTful CRUD API Sample with PHP + SQLite(PDO) + BasicAuth
 * 
 * Server$ php -S 127.0.0.1:8080 router.php
 * Client$ curl -v http://127.0.0.1:8080/ | jq
 * Client$ curl -v -u "admin:nimda" -X GET http://127.0.0.1:8080/api/v1/user/ | jq
 */
require_once 'rest.php';


$user = (isset($_SERVER['PHP_AUTH_USER'])) ? $_SERVER['PHP_AUTH_USER'] : NULL;
$pass = (isset($_SERVER['PHP_AUTH_PW'])) ? $_SERVER['PHP_AUTH_PW'] : NULL;
$method = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];


$rest = new Rest();
$rest->handle_request($user, $pass, $method, $uri);

?>
